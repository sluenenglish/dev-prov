#!/usr/bin/env bash

###### CONFIG ######
export USER="sam"
export USER_HOME="/home/$USER"


###### UTILS ######
safe_clone(){
  [ -d "$2" ] || git clone "$1" "$2"
}

declare -A colours
colours=(
    ["black"]="0"
    ["red"]="1"
    ["green"]="2"
    ["yellow"]="3"
    ["blue"]="4"
    ["magenta"]="5"
    ["cyan"]="6"
    ["white"]="7"
)

cprint(){
  colour=$(tput setaf ${colours[$1]})
  end=$(tput sgr 0)
  shift
  text="$@"
  printf "$colour$text$end"
}

cecho(){
  echo "$(cprint $@)"
}

bprint(){
  bold=$(tput bold)
  end=$(tput sgr 0)
  text="$@"
  printf "$bold$text$end"
}


green_tick="$(cecho green '✓')"


divert() {
  tmp=$(mktemp) || return # this will be the temp file w/ the output
  "$@" # > "$tmp" 2>&1 # this should run the command, respecting all arguments
  ret=$?
  if [ "$ret" -eq 0 ] ;
  then
    printf "$green_tick"
    return "$ret"
  else
    echo
    cat "$tmp"
    cecho red "An error occurred, exiting..."
    rm -f "$tmp"
    exit "$ret"
  fi
}


printf "Install yay "
function install_yay() {
    [ -d "/tmp/yay/" ] || git clone https://aur.archlinux.org/yay-git.git /tmp/yay/
	cd /tmp/yay
	chown -R $USER:$USER .
	sudo --user=$USER -s makepkg --noconfirm -si
}
divert install_yay
echo

###### PACKAGES ######
printf "Updating package index  "
divert pacman -Syu
divert yay -Syu
echo

printf "Installing packages  "
packages=(
python-pip
vim
tmux
git
curl
zsh
tig
xclip
caffeine
powerline-fonts
htop
nodejs
npm
yarn
python-black
prettier
docker
peek
tldr
)

for p in "${packages[@]}"; do
   divert pacman  -S --needed --noconfirm $p
done

packages=(
pycharm-professional
)

for p in "${packages[@]}"; do
   divert sudo --user=sam --set-home bash -c "yay  -S --noconfirm $p"
done

divert yarn global add n
echo

groupadd docker
usermod -aG docker $USER
newgrp docker 
systemctl start docker.service
systemctl enable docker.service


###### ZSH ######
printf "Installing oh-my-zsh "
chsh -s $(which zsh) $USER
function install_zsh() {
  [ -d "$USER_HOME/.oh-my-zsh/" ] || sudo --user=sam --set-home bash -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
}
divert install_zsh
echo


###### DOTFILES ######
printf "Linking dotfiles "
divert ln -fL $USER_HOME/dev-prov/dotfiles/zshrc $USER_HOME/.zshrc
divert ln -fL $USER_HOME/dev-prov/dotfiles/ideavimrc $USER_HOME/.ideavimrc
divert ln -fL $USER_HOME/dev-prov/dotfiles/tmux.conf $USER_HOME/.tmux.conf
divert ln -fL $USER_HOME/dev-prov/dotfiles/vimrc $USER_HOME/.vimrc
divert ln -fL $USER_HOME/dev-prov/dotfiles/zshrc $USER_HOME/.zshrc
divert ln -fL $USER_HOME/dev-prov/dotfiles/gitconfig $USER_HOME/.gitconfig
divert ln -fL $USER_HOME/dev-prov/dotfiles/udevmon.yaml /etc/udevmon.yaml
divert ln -fL $USER_HOME/dev-prov/dotfiles/udevmon.service /etc/systemd/system/udevmon.service
divert ln -fL $USER_HOME/dev-prov/dotfiles/work_config $USER_HOME/.work_config
echo


###### VUNDLE ######
printf "Installing vundle "
divert safe_clone https://github.com/VundleVim/Vundle.vim.git $USER_HOME/.vim/bundle/Vundle.vim
chown -R $USER:$USER $USER_HOME/.vim/bundle/
sudo --user=$USER --set-home vim +PluginInstall +qall
echo


###### TMUX ######
printf "Install TPM and install plugins "
divert safe_clone https://github.com/tmux-plugins/tpm $USER_HOME/.tmux/plugins/tpm 
divert chown -R $USER:$USER $USER_HOME/.tmux
divert sudo --user=$USER --set-home --login '$HOME/.tmux/plugins/tpm/bin/install_plugins'
divert sudo --user=$USER --set-home --login bash -c '$HOME/.tmux/plugins/tpm/bin/update_plugins all'
echo


##### BASE16 ######
printf "Install BASE16 "
divert safe_clone https://github.com/chriskempson/base16-shell.git $USER_HOME/.config/base16-shell
echo

###### KEYBOARD ######
printf "Configure keyboard "
divert sudo --user=$USER yay -S --noconfirm interception-tools

divert pip install pykeymapper
divert systemctl enable --now udevmon
divert systemctl restart udevmon
if ! grep -qxF 'XKBOPTIONS="ctrl:nocaps"' /etc/default/keyboard > /dev/null; then
  divert echo 'XKBOPTIONS="ctrl:nocaps"' >> /etc/default/keyboard
else
  cprint yellow '-'
fi
echo


cecho green "$(bprint 'Provisioned succesfully 👍 ')"
